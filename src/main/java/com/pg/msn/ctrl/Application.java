package com.pg.msn.ctrl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pg.msn.ctrl.alrt.BattAlert;
import com.pg.msn.ctrl.alrt.TempAlert;


/*******************************************************************
 * REQUIREMENTS:
 * Ingest status telemetry data and create alert messages for the following violation conditions:
 * 
 * 1). If for the same satellite there are three battery voltage readings that 
 *     are under the red low limit within a five minute interval.
 * 2). If for the same satellite there are three thermostat readings that 
 *      exceed the red high limit within a five minute interval.
************************************************************************/
/**********************************************
 * <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
 *********************************************/
public class Application {
	
    String RED_LOW = "RED LOW";
    String RED_HIGH = "RED HIGH";
    String PIPE = "|";
    String BLANK = "";
    String ZULU = "Z";
    String TIMESTAMP_PATTERN = "yyyyMMdd HH:mm:ss.SSS";
    int BATTERY_RED_LOW = 8;
    int THERMOSTAT_RED_HIGH = 101;
    int MAX_ALERTS = 3;
    String TSTAT = "TSTAT";
    String BATT = "BATT";
    
    
	// Readings.txt - Input Data
	private static final String DATA_FILE = "data/Readings.txt";

	private String inputData;
	private LocalDateTime timestamp;
	private int satelliteId;
	private int redHighLimit;
	private int yellowHighLimit;
	private int yellowLowLimit;
	private int redLowLimit;
	private float rawValue;
	private String component;

	
	private final TempAlert temperatureAlert = new TempAlert();
	private final BattAlert batteryAlert = new BattAlert();

	public Application() {
	}

	public Application(String inputData) {
		this.inputData = inputData;
	}

	public Application(LocalDateTime timestamp, int satelliteId, int redHighLimit, int yellowHighLimit,
			int yellowLowLimit, int redLowLimit, float rawValue, String component) {
		this.timestamp = timestamp;
		this.satelliteId = satelliteId;
		this.redHighLimit = redHighLimit;
		this.yellowHighLimit = yellowHighLimit;
		this.yellowLowLimit = yellowLowLimit;
		this.redLowLimit = redLowLimit;
		this.rawValue = rawValue;
		this.component = component;
	}

	/******************************************
	 * Main method: Application entry point
	 *********************************************/
	public static void main(String[] args) {
		
		Application app = new Application();
				
		try {
			app.readDataFile();
			
		} catch (FileNotFoundException e) {
			System.out.println("Data file not found: " + e.getMessage());
			
		}		
	}


	/******************************
	 * Read and process the input data
	 ****************************/
	private void readDataFile() throws FileNotFoundException {
		List<BattAlert> batteryAlertList = new ArrayList<>();
		List<TempAlert> tempAlertList = new ArrayList<>();
		int batAlertCounter = 0;
		int tempAlertCounter = 0;

		System.out.println(BLANK);
		InputStream inputStream = Application.class.getClassLoader().getResourceAsStream(DATA_FILE);
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		Scanner read = new Scanner(br);

		while (read.hasNextLine()) {
			// read a line of data
			setInputData(read.nextLine());
			// parse the line of data
			parseData();
			
			if (batAlertCounter < MAX_ALERTS) { 
				// If three BATT alerts haven't been detected yet
				if (getComponent().equals(BATT) && getRedLowLimit() == BATTERY_RED_LOW) {
					batAlertCounter++;
					if (batAlertCounter == MAX_ALERTS) {
						batteryAlert.setSatelliteId(Integer.toString(getSatelliteId()));
						batteryAlert.setSeverity(RED_LOW);
						batteryAlert.setComponent(getComponent());
						batteryAlert.setTimestamp(timestampToString(getTimestamp()));
						batteryAlertList.add(batteryAlert);
					}
				}
				
			}

			if (tempAlertCounter < MAX_ALERTS) { // If three TSTAT alerts haven't been detected yet
				if (getComponent().equals(TSTAT) && getRedHighLimit() == THERMOSTAT_RED_HIGH) {
					tempAlertCounter++;
					if (tempAlertCounter == MAX_ALERTS) {
						temperatureAlert.setSatelliteId(Integer.toString(getSatelliteId()));
						temperatureAlert.setSeverity(RED_HIGH);
						temperatureAlert.setComponent(getComponent());
						temperatureAlert.setTimestamp(timestampToString(getTimestamp()));
						tempAlertList.add(temperatureAlert);
					}
				}
				
			}

			if (batAlertCounter == MAX_ALERTS && tempAlertCounter == MAX_ALERTS) {
				displayAlerts(batteryAlertList, tempAlertList);
				break;
			}
		}
		read.close();
	}

	/*********************************************************
	 * Parses the data for each line read into separate fields
	 ***********************************************************/
	private void parseData() {
		List<String> dataList = new ArrayList<>();
		StringTokenizer st = new StringTokenizer(getInputData(), PIPE);
		while (st.hasMoreTokens()) {
			dataList.add(st.nextToken());
		}
		setTimestamp(convertTimestampString(dataList.get(0), TIMESTAMP_PATTERN));
		setSatelliteId(Integer.parseInt(dataList.get(1)));
		setRedHighLimit(Integer.parseInt(dataList.get(2)));
		setYellowHighLimit(Integer.parseInt(dataList.get(3)));
		setYellowLowLimit(Integer.parseInt(dataList.get(4)));
		setRedLowLimit(Integer.parseInt(dataList.get(5)));
		setRawValue(Float.valueOf(dataList.get(6)));
		setComponent(dataList.get(7));
	}

	/******************************************************
	 * To format the alert message in JSON and display the messages on the console.
	 ********************************************************/
	public void displayAlerts(List<BattAlert> batteryAlertList, List<TempAlert> tempAlertList) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();		
		System.out.println("* * * * A-L-E-R-T * * * *\n");
		// Print Temperature alert message
		for (TempAlert temperatureAlerts : tempAlertList) {
			System.out.println(gson.toJson(temperatureAlerts));
		}
		
		// Print Battery alert message
		for (BattAlert batteryAlerts : batteryAlertList) {
			System.out.println(gson.toJson(batteryAlerts));
		}
	}
	
	
	public LocalDateTime convertTimestampString(String timestamp, String pattern) {
		return LocalDateTime.parse(timestamp, DateTimeFormatter.ofPattern(pattern));
	}

	public String timestampToString(LocalDateTime localDateTime) {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(TIMESTAMP_PATTERN);
		return localDateTime.format(dateTimeFormatter) + ZULU;
	}

	public String getInputData() {
		return inputData;
	}

	public void setInputData(String inputData) {
		this.inputData = inputData;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public int getSatelliteId() {
		return satelliteId;
	}

	public void setSatelliteId(int satelliteId) {
		this.satelliteId = satelliteId;
	}

	public int getRedHighLimit() {
		return redHighLimit;
	}

	public void setRedHighLimit(int redHighLimit) {
		this.redHighLimit = redHighLimit;
	}

	public int getYellowHighLimit() {
		return yellowHighLimit;
	}

	public void setYellowHighLimit(int yellowHighLimit) {
		this.yellowHighLimit = yellowHighLimit;
	}

	public int getYellowLowLimit() {
		return yellowLowLimit;
	}

	public void setYellowLowLimit(int yellowLowLimit) {
		this.yellowLowLimit = yellowLowLimit;
	}

	public int getRedLowLimit() {
		return redLowLimit;
	}

	public void setRedLowLimit(int redLowLimit) {
		this.redLowLimit = redLowLimit;
	}

	public float getRawValue() {
		return rawValue;
	}

	public void setRawValue(float rawValue) {
		this.rawValue = rawValue;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

}
