package com.pg.msn.ctrl.alrt;

public class BattAlert {

	private String satelliteId;
	private String severity;
	private String component;
	private String timestamp;

	public String getSatelliteId() {
		return satelliteId;
	}

	public void setSatelliteId(String satelliteId) {
		this.satelliteId = satelliteId;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public BattAlert() {
	}

	public BattAlert(String satelliteId, String severity, String component, String timestamp) {
		this.satelliteId = satelliteId;
		this.severity = severity;
		this.component = component;
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "Alerts{" + "satelliteId=" + satelliteId + ", severity='" + severity + '\'' + ", component='"
				+ component + '\'' + ", timestamp=" + timestamp + '}';
	}

}
